import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as glob from 'glob';
import { isUndefined, isNull, TextDecoder } from 'util';
import { promises } from 'dns';
import * as bdd_funcs from './bdd_funcs';
import * as pytest from './pytest';
import { parse, resolve } from 'path';
import { SIGVTALRM, POINT_CONVERSION_COMPRESSED } from 'constants';
import { get } from 'http';
import * as lib from './mylib';

export class TestItemProvider implements vscode.TreeDataProvider<TestItem> {
	private _item = new Map();
	private _onDidChangeTreeData: vscode.EventEmitter<TestItem | undefined> = new vscode.EventEmitter<TestItem | undefined>();
	readonly onDidChangeTreeData: vscode.Event<TestItem | undefined> = this._onDidChangeTreeData.event;
	public term: vscode.Terminal | null= null;
	public view: vscode.TreeView<TestItem> |undefined;
	public action = actionTypes.idle;
	private results = new Map();
	private outputs = new Map();
	private running_scenarios: TestItem[] = [];
	private scenarios_to_run: TestItem[] = [];
	private only_bdd: boolean = false;
	public coms: vscode.Disposable[] =[];
	public coollected_tests: string = '';
	public last_path: string = '';

	constructor() {
		
	}

	private onActiveEditorChanged(): void {
	}

	public clear_results(){
		this.running_scenarios = []
		this.results.clear();
	}

	public state_is_idle(){
		return this.action === actionTypes.idle;
	}

	getParent(element: TestItem){
		return element;
	}

	public collapseAll(){
	}

	public includes_tests_in_subdirs(path: string | undefined ): boolean{
		if(path === undefined){ return false;}
		var bool =  pytest.is_any_python_test_in_subdirs([path]);
		if(this.only_bdd){
			bool = bool && pytest.is_any_feature_in_subdirs([path]);
		}
		return bool;
	}

	public expandAll(){
			var item = this.scenarios_to_run[0];

			if(item === undefined){return;}
			item.type = itemType.loadingScenario;
			
			this.refreshItem(item);
			this.exec_command(item);
	}

	public get_test_type(item: TestItem): itemType {
		var type = itemType.scenario;
		for(var i=0; i < this.running_scenarios.length; i+=1){
			var it = this.running_scenarios[i];
			if(it.exec_str === item.exec_str){type = itemType.loadingScenario;}
		}

		if(this.results.get(item.exec_str) !== undefined){
			type = this.results.get(item.exec_str);
		}
		return type;
	}

	public is_item_running(item: TestItem): boolean{
		for(var i=0; i < this.running_scenarios.length; i+=1){
			var it = this.running_scenarios[i];
			if(it.exec_str === item.exec_str){return true;}
		}
		return false;
	}
	
	public remove_item_from_running_scenarios(item: TestItem): boolean{
		for(var i=0; i < this.running_scenarios.length; i+=1){
			var it = this.running_scenarios[i];
			if(it.exec_str === item.exec_str){
				const index = this.running_scenarios.indexOf(it, 0);
				if (index > -1) {
				this.running_scenarios.splice(index, 1);
				}
				return true;
			}
		}
		return false;
	}

	public switch_bdd_filter(){
		if(!this.state_is_idle()){return;}
		this.only_bdd = !this.only_bdd;
		this._onDidChangeTreeData .fire(undefined);
	}

	refreshItem(item: TestItem| undefined): void {
		this._onDidChangeTreeData .fire(item);
	}

	getTreeItem(element: TestItem): vscode.TreeItem {
		return element;
	}
	
	public find_item(path: string, item: TestItem | undefined = undefined): TestItem {
		var childs = this.getChildrenSync(item);
		for (let index = 0; index < childs.length; index++) {
			const child = childs[index];
			if (child.path === path){
				return child;
			} else if (path.indexOf(child.path) === 0) {
				return this.find_item(path, child);
			}
		}
		return new TestItem('', itemType.none, '');
	}


	public collect_all_tests(mark: string = ""): string{
    	var root = vscode.workspace.rootPath;
		if(root !== undefined)
		{
			this.coollected_tests = pytest.collect_tests(root, mark);
		}
		return this.coollected_tests
	}
	public collect_all_tests_async(mark: string = ""): Thenable<string>{
    	var root = vscode.workspace.rootPath;
		if(root !== undefined)
		{
			var proms: Thenable<string>[] = [];
			var paths = pytest.dirs_and_pythons_in_dir(root);

			paths.forEach(path => {
				proms.push(pytest.collect_tests_async(root +'\\' + path, mark));
			});

			return Promise.all(proms).then(prs => {
				this.coollected_tests = '';
				prs.forEach(text=>{
					this.coollected_tests += text;
				});
				bdd_funcs.output.append("Pytest-BDD: Test Discovery Completed!!!\n");
				return this.coollected_tests;
			});
		}
		return Promise.resolve('');
	}

	private get_collected_tests_for_python(path: string): string [] {
		var root = vscode.workspace.rootPath?.replace(/\\/g, '/').replace('C:', 'c:') + '//';
    	var text = this.coollected_tests;
		var lines = text.split('\r\n');
		var results: string [] =[];
		var found = false;
		var line_path = '';
		var module = '';
		var packag = '';
		for (let i = 0; i < lines.length; i++) {
			const line = lines[i];
			var func = '';
			if( found && (line.indexOf('Function') < 0)){
				// return results;
				found = false;
			}

			if(found){
				results.push(line.replace('<Function', '').replace('>','').trim());
			}

			if(line.indexOf('Package') >= 0){
				packag = line.replace('<Package', '').replace('>', '') + '\\';
			}

			if(line.indexOf('Module') >= 0){
				module = line.replace('<Module', '').replace('>', '').trim();
				if (!module.startsWith('  ')){packag = root;}
			}

			if(line.indexOf('Function') >= 0){
				func = line.replace('<Function', '').replace('>', '');
				
			}

			line_path = module;
			line_path = line_path.replace(/\\/g, '/').replace(/\/\//g, '/').replace('C:', 'c:').trim();

			// if(line_path === path){
			if(path.indexOf(line_path) >= 0 && module.length > 0 
				&& path.indexOf(line_path) + line_path.length == path.length){
				found = true;
			}

			if(line.indexOf('big event not triggered')>=0){
				var x  = true;
			}
		}
		return results.sort();
	}

	private save_python_tests_async(item: TestItem) {
		var proms = [];

		proms.push(new Promise((resolve, reject) => {
			if(item.pythonPath === null){ return [];}
			var py_tests = this.get_collected_tests_for_python(item.pythonPath);
			item.wall.set('collect_tests', py_tests);
			resolve(true);
		}));

		proms.push(new Promise((resolve, reject) => {
			if(item.pythonPath === null){ return [];}
			var python_tests = pytest.scenarios_in_python(item.pythonPath);
			item.wall.set('python_tests', python_tests);
			resolve(true);
		}));

		if(item.isPython){
			proms.push(new Promise((resolve, reject) => {
				if(item.pythonPath === null){ return [];}
				var features = pytest.features_in_python(item.pythonPath);
				item.wall.set('features', features);
				resolve(true);
			}));
		}

		proms.push(new Promise((resolve, reject) => {
			if(item.pythonPath === null){ return [];}
			var duplicates = pytest.duplicates_in_python(item.pythonPath);
			item.wall.set('duplicates', duplicates);
			resolve(true);
		}));
	}

	private get_features_in_python(item: TestItem): pytest.Feature[] {
		var features: pytest.Feature[] = item.wall.get('features');
		if(features == undefined){
			features = pytest.features_in_python(item.path);
		}
		return features;
	}

	private get_collect_test_list(item: TestItem): string [] {
		item.wall.set('collect_tests', '');
		var tests: string [] = item.wall.get('collect_tests');
		if(tests.length === 0){
			if (this.coollected_tests.length === 0){
				tests = pytest.collect_test_list(item.pythonPath);
			}
			else {
				if(item.pythonPath === null){return [];}
				tests = this.get_collected_tests_for_python(item.pythonPath);
			}
		}
		return tests.sort();
	}

	private get_scenarios_in_python(item: TestItem): pytest.Scenario [] {
		item.wall.set('collect_tests', '');
		var tests: pytest.Scenario [] = item.wall.get('python_tests');
		if(tests == undefined){
			tests = pytest.scenarios_in_python(item.path);
		}
		return tests;
	}
	
	private get_duplicates_in_python(item: TestItem): pytest.Duplicate[] {
		var tests: pytest.Duplicate[] = item.wall.get('duplicates');
		if(tests === undefined){
			tests = pytest.duplicates_in_python(item.pythonPath);
		}
		return tests;
	}

	getChildrenSync(elem?: TestItem): TestItem[]{
		var items: TestItem[] = [];
		
		if(elem == undefined || elem.isFolder){ 
			var path = isUndefined(elem)?vscode.workspace.rootPath:elem.path;
			path = path?.replace(/\\/g, '/');
			
			var folders = pytest.folders_in_dir(path).sort();
			folders.forEach(folder=>{
				var p = path + '/' + folder;
				if (this.includes_tests_in_subdirs(p)){
					var type =  itemType.folder;
					var t = new TestItem(folder, type, p);
					if(this.is_busy){t.collapsibleState = vscode.TreeItemCollapsibleState.Expanded;}
					if(this.last_path.indexOf(t.path + '/') === 0){
						t.collapsibleState = vscode.TreeItemCollapsibleState.Expanded;
					}
					items.push(t);
				}
			});

			var pythons = pytest.pythons_in_dir(path).sort();
			pythons.forEach(python=>{
				if (this.includes_tests_in_subdirs(path)){
					if(!python.startsWith('test_')){return;}
					var p = path + '/' + python;
					var type =  itemType.python;
					var t = new TestItem(python, type, p, p);
					if(this.is_busy){t.collapsibleState = vscode.TreeItemCollapsibleState.Expanded;}
					if(this.last_path.indexOf(t.path) === 0){
						t.collapsibleState = vscode.TreeItemCollapsibleState.Expanded;
					}
					var features = this.get_features_in_python(t);
					features.forEach(feature=>{
						if(this.last_path.indexOf(feature.path) === 0){
							t.collapsibleState = vscode.TreeItemCollapsibleState.Expanded;
						}
					});
					items.push(t);
					var prom: Promise<boolean> = new Promise((resolve, reject) => {
						this.save_python_tests_async(t);
					});
				}
			});

		} else if(elem.isPython){

			var features = this.get_features_in_python(elem);
			features.forEach(feature=>{
				var type =  itemType.feature;
				var t = new TestItem(feature.name, type, feature.path, elem.path, feature.path);
				t.collapsibleState = vscode.TreeItemCollapsibleState.Expanded;
				items.push(t);
				t.wall.set('collect_tests', elem.wall.get('collect_tests'));
				t.wall.set('duplicates', elem.wall.get('duplicates'));
			});
			
			var py_tests = this.get_collect_test_list(elem);
			var python_tests = this.get_scenarios_in_python(elem);
			var duplicates = this.get_duplicates_in_python(elem);
			for (let i = 0; i < py_tests.length; i++) {
				const py_test = py_tests[i];
				var final_scen: pytest.Scenario;
				for (let i = 0; i < python_tests.length; i++) {
					const scen = python_tests[i];
					if(scen.name === null){continue;}

					var isDuplicated = false;
					for (let i = 0; i < duplicates.length; i++) {
						const dup = duplicates[i];
						if(dup.testName === scen.name){
							isDuplicated = true;
							break;
						}
					}
					var not_parametrized_name = py_test;
					if(py_test.indexOf('[')>=0 && py_test.indexOf(']')>=0){
						not_parametrized_name = py_test.split('[')[0].trim();
					}

					if(not_parametrized_name === scen.name && !isDuplicated){
						final_scen = new pytest.Scenario(scen.line, py_test, null, py_test);
						var type =  itemType.scenario;
						if(final_scen.name === null){continue;}
						var t = new TestItem(final_scen.name, type, elem.path, elem.path, null, final_scen);
						t.type = this.get_test_type(t)
						items.push(t);
						break;
					}
				}
			}
		}else if(elem.isFeature){
			var feature_tests = pytest.scenarios_in_feature(elem.featurePath);
			var py_tests = this.get_collect_test_list(elem);
			var duplicates = this.get_duplicates_in_python(elem);
			elem.childs = [];
			py_tests.forEach(py_test=>{
				var final_scen: pytest.Scenario;
				feature_tests.forEach(scen =>{
					if(scen.name === null){return;}

					var isDuplicated = false;
					var dupName = '';
					for (let i = 0; i < duplicates.length; i++) {
						const dup = duplicates[i];
						if(dup.scenarioName === scen.label){
							isDuplicated = true;
							dupName = dup.testName
							break;
						}
					}
					
					var not_parametrized_name = py_test;
					if(py_test.indexOf('[')>=0 && py_test.indexOf(']')>=0){
						not_parametrized_name = py_test.split('[')[0].trim();
					}

					// if(py_test.indexOf(scen.name)>=0 && !isDuplicated){
					// if(not_parametrized_name === 'test_' + scen.name && !isDuplicated){
					if(not_parametrized_name === 'test_' + scen.name ||
						(isDuplicated && dupName == not_parametrized_name )) {
						// final_scen = new pytest.Scenario(scen.line, py_test);
						final_scen = new pytest.Scenario(scen.line, scen.label, scen.feature);
						final_scen.name = py_test;
						if (py_test.split('[').length === 2){
							final_scen.label += ' [' + py_test.split('[')[1];
						}
						
						var type =  itemType.scenario;
						
						if(final_scen.name === null){return;}
						var t = new TestItem(final_scen.label, type, elem.path, elem.pythonPath, null, final_scen);
						t.type = this.get_test_type(t)
						items.push(t);
					}
				});
			});	
	
		}	else if(elem.isScenario){
			elem.type =  this.get_test_type(elem);
		}

		if(elem != undefined){
			elem.childs = items;
			this._item.set(elem?.exec_str, elem);
		}
		return items;
	}

	getChildren(elem?: TestItem): Thenable<TestItem[]> {

		return Promise.resolve(this.getChildrenSync(elem));
	}

	public run_explorer_test_sync(parent: TestItem){
		try{
			this.action = actionTypes.busy;
			this.running_scenarios = [];
			this.view?.reveal(parent, {select:true, focus:true, expand: true});
			this.get_running_scenarios(parent);
			this.scenarios_to_run = Object.assign([], this.running_scenarios);
			this.results.clear();
			vscode.commands.executeCommand('bdd.expandAll');
		}catch{
			this.action = actionTypes.idle;
		}
	}

	public debug_explorer_test(item: TestItem){
		if(this.action !== actionTypes.idle){return;}
		if(!item.isScenario){return;}
		this.action = actionTypes.busy;
		this.view?.reveal(item, {select:true, focus:true, expand: true});
		this.debug_test(item.exec_str);
	}

	private exec_command(item: TestItem): Thenable<boolean>{
		var comm = 'pytest "' + item.exec_str + '"';
		var exec = require('child_process').exec;
		var tree = this;
		
		var root = vscode.workspace.rootPath;
		comm = 'cd ' + root + ' & ' + comm;

		var prom: Promise<boolean> = new Promise((resolve, reject) => {
			exec(comm, tree, function(error: string, stdout: string, stderr: string) {
			// command output is in stdout
			const stdout_all = stdout;

			var stdout_bdd = '';
			var match = stdout.match(/#########   Scenario Failed:(.|\r\n)*File:.*feature/);
			if (match !== null){
				stdout_bdd = match[0];
			}

			var show_prop:  string | undefined = vscode.workspace.getConfiguration().get("PytestBdd.output2remove");
			if (show_prop === undefined){
				show_prop = '';
			}
			var show_prop_arr: string[] = show_prop.split(',');
			var stdout_call_removed = false;
			show_prop_arr.forEach(prop => {
				var stdout_to_remove = '';
				const pattern = '----* ' + prop + ' ----*\r\n(([^-].*\r\n)|(\r\n)*)*';
				var match = stdout.match(pattern);
				if (match !== null){
					const last_line = match[0].split('\r\n')[match[0].split('\r\n').length-1];
					stdout_to_remove = match[0].replace(last_line, '');
					if( prop === "Captured stdout call"){
						// stdout = stdout.replace(RegExp(stdout_to_remove, 'g'), stdout_bdd);
						stdout = stdout.replace(stdout_to_remove, '');
						stdout_call_removed = true;
						
					}
					else{
						stdout = stdout.replace(stdout_to_remove, '');
					}
				}
			});
			if (stdout_call_removed){
				stdout += '\n' + stdout_bdd;
			}

			bdd_funcs.output.append(stdout);
			// bdd_funcs.output.append(stdout_bdd);
			// bdd_funcs.output.show();
			
			tree.outputs.set(item.exec_str, stdout);
			tree.results.set(item.exec_str, itemType.scenario);

			if(stdout_all.indexOf('passed') >= 0){tree.results.set(item.exec_str, itemType.passedScenario);}
			if(stdout_all.indexOf('skipped') >= 0){tree.results.set(item.exec_str, itemType.skippedScenario);}
			if(stdout_all.indexOf('failed') >= 0){tree.results.set(item.exec_str, itemType.failedScenario);}

			var type =itemType.scenario;
			if(stdout_all.indexOf('==== 1 passed') >= 0){type = itemType.passedScenario;}
			if(stdout_all.indexOf('==== 1 skipped') >= 0){type = itemType.skippedScenario;}
			if(stdout_all.indexOf('==== 1 failed') >= 0 || stdout.indexOf('==== 1 error') >= 0 ){type = itemType.failedScenario;}

			item.type = type;

			tree.refreshItem(item);
			tree.scenarios_to_run.shift();
			if ( tree.scenarios_to_run.length >= 1){
				vscode.commands.executeCommand('bdd.expandAll');
			}else{
				tree.action = actionTypes.idle;
			}

			if (stdout ===''){reject(stderr);}
			resolve(true);
			});
		});
		return prom;
	}

	public debug_test(exec_str: string|undefined){
		var root_path =  this.get_root_folder()?.uri.fsPath + '\\';
		root_path = bdd_funcs.format_test_path(root_path);

		var opt = {
			name: "Python: pytest",
			type: "python",
			console: "internalConsole",
			justmycode: false,
			noDebug:false,
			request: "launch",
			module: "pytest",
			args: ["--log-file", root_path +  bdd_funcs.log_file,
				   "--log-file-level", "DEBUG",
				   "--log-file-format", "%(asctime)s-[%(levelname)8s]-%(message)s-(%(filename)s:%(lineno)s)",
				   "--log-file-date-format", "%Y-%m-%d_%H:%M:%S",
				   "--debug",
				//    "--no-print-logs",
				   exec_str,
				],

		};
		this.results.set(exec_str, itemType.scenario);
		return vscode.debug.startDebugging(this.get_root_folder(), opt, );		
	}

	private debugSessionStopedListener = vscode.debug.onDidTerminateDebugSession( ds => {
		this.debugSessionStoped();
	});

	private debugSessionStoped(get_from_output=false){
		this.action = actionTypes.idle;
	}

	public stopDebuging(){
		this.scenarios_to_run = [];
		this.running_scenarios = [];
	}

	private get_running_scenarios(parent: TestItem): TestItem[] {
		if(parent.isScenario){
				this.running_scenarios.push(parent);
		}else{
			var childs: TestItem[] = [];
			
			if(!isUndefined(this._item.get(parent.exec_str)))
			{
				childs = this._item.get(parent.exec_str).childs;
			}
			else
			{
				childs = this.getChildrenSync(parent);
			}

			childs.forEach(child => {
				if(child.isScenario){
					
					this.running_scenarios.push(child);
				}else {
					this.get_running_scenarios(child);
				}
				
			});
		}

		return this.running_scenarios;
	}

	get is_busy(): boolean {
		return this.action === actionTypes.busy;
	}

	get is_idle(): boolean {
		return this.action === actionTypes.idle;
	}

	private tree_change = this.onDidChangeTreeData(item => {
		
	});

	public get_root_folder() {
		if (isUndefined(vscode.workspace.rootPath)) {return;}
		var uri = vscode.Uri.file(vscode.workspace.rootPath);
		return vscode.workspace.getWorkspaceFolder(uri);
	}

	public go_to_source(item: TestItem){
		if (item.type === itemType.folder) { return;}
		var line: number = item.scenario === null?0:item.scenario.line;
		bdd_funcs.show_doc(item.path, 1).then(te=>{
			bdd_funcs.select_focus_line(te, line);
		});
	}
	
	private pathExists(p: string): boolean {
		try {
			fs.accessSync(p);
		} catch (err) {
			return false;
		}

		return true;
	}

	public item_selected(item: TestItem){
		const out_text = this.outputs.get(item.exec_str);
		if ( out_text !== "" && out_text !== undefined){
			bdd_funcs.output.clear();
			bdd_funcs.output.append(out_text);
			bdd_funcs.output.show();		
		}
		this.refreshItem(item);
		this.view?.reveal(item, {select:true, focus:true, expand: true});
	    this.go_to_source(item);
	}
}

export class TestItem extends vscode.TreeItem {
	public collapsibleState: vscode.TreeItemCollapsibleState =1;
	public childs: TestItem[] = [];
	public exec_str: string ='';
	public isRoot: boolean = false;
	public wall: Map<string, any> = new Map();
	constructor
	(
		public label: string,
		public type: string,
		public path: string,
		// public command?: vscode.Command,
		public pythonPath: string | null = null,
		public featurePath: string | null = null,
		public scenario: pytest.Scenario | null = null,
		public contextValue?: string,
		public term_output?: string,

	) {
		super(label, 1);
		if(this.isScenario){
			this.collapsibleState = 0;
			this.exec_str = this.pythonPath + '::' + this.scenario?.name;
		}else{
			this.exec_str = this.path;
		}
	}

	command = {
			command: "bdd.item_selected",
			title: "Select Item",
			arguments: [this]
		};
	

	get tooltip(): string {
		return `${this.label}-${this.type}`;
	}

	get description(): string {
		return '';
	}

	get icon():string{
		var svg = 'folder.svg';
		if( this.type === itemType.scenario){
			svg = 'test.svg';
		} else if ( this.type ===itemType.loadingScenario){
			svg = 'loading.svg';
		} else if ( this.type ===itemType.passedScenario){
			svg = 'passed.svg';
		} else if ( this.type ===itemType.skippedScenario){
			svg = 'skipped.svg';
		} else if ( this.type ===itemType.failedScenario){
			svg = 'failed.svg';
		} else if (  this.type === itemType.python){
			svg = 'python.svg';
		} else if (  this.type === itemType.feature){
			svg = 'cucumber.svg';
		}
		return svg;
	}

	get iconPath(){
		var iconPath = {
			light: path.join(__filename, '..', '..', 'resources', 'light', this.icon),
			dark: path.join(__filename, '..', '..', 'resources', 'dark', this.icon)
		};
		return iconPath;
	}

	get isBdd(): boolean{
		return !isNull(this.featurePath);
    }
	
	get isFolder(){
		return this.type === itemType.folder;
	}

	get isFeature(){
		return this.type === itemType.feature;
	}

	get isPython(){
		return this.type === itemType.python;
	}

	get isScenario(): boolean{
		if ( this.type ===itemType.scenario
			|| this.type ===itemType.loadingScenario
			|| this.type ===itemType.passedScenario
			|| this.type ===itemType.skippedScenario
			|| this.type ===itemType.failedScenario
			){return true;}

		return false;
	}

}

export class TestResult {
	constructor(
		public name: string,
		public result: string,
	){
	}

}

enum itemType {
	loadingScenario = 'loading-scenario',
	passedScenario = 'passed-scenario',
	failedScenario = 'failed-scenario',
	skippedScenario = 'skipped-scenario',
	scenario = 'scenario',
	feature = 'feature',
	python = 'python',
	folder = 'folder',
	none = '',
}

export enum actionTypes {
	idle = "",
	busy = "busy",
	debug = "  --  Debugging . . .",
	run = "  --  Running . . .",
}

