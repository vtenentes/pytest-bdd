/*---------------------------------------------------------
 * Copyright (C) Microsoft Corporation. All rights reserved.
 *--------------------------------------------------------*/

import * as vscode from 'vscode';
import { legend, DocumentSemanticTokensProvider } from './scemanticToken';
import { TestItemProvider, actionTypes, TestItem} from './testExplorer';
import { RefItemProvider} from './refsExplorer';
import * as bdd_funcs from './bdd_funcs';
import {create_step} from './new_step';
import { isUndefined } from 'util';
import {test_function} from './pytest';


var searchDecs: bdd_funcs.Search;
export var testProv = new TestItemProvider();
export var refsProv = new RefItemProvider();

export function activate(context: vscode.ExtensionContext) {
	// Search for BDD Steps and Decorators
	if(!searchDecs){
		searchDecs = bdd_funcs.search_step_files_for_decs();
	}
	// Commands
	vscode.commands.registerCommand('bdd.search_for_steps', () => {
		searchDecs = bdd_funcs.search_step_files_for_decs();
	});
	vscode.commands.registerCommand('bdd.go2def', () => {
		bdd_funcs.go_to_step_definition(searchDecs.step_decs);
	});
	vscode.commands.registerCommand('bdd.run_file', () => {
		bdd_funcs.run_file();
	});
	vscode.commands.registerCommand('bdd.debug_file', () => {
		bdd_funcs.debug_file();
	});
	vscode.commands.registerCommand('bdd.run_scenario', () => {
		bdd_funcs.run_scenario();
	});
	vscode.commands.registerCommand('bdd.debug_scenario', () => {
		bdd_funcs.debug_scenario();
	});
	vscode.commands.registerCommand('bdd.find_references', () => {
		bdd_funcs.find_references().then( results =>{
			init_refs_explorer();
			if (results !== undefined){refsProv.results = results;}
		}); 
	});
    vscode.commands.registerCommand('bdd.refreshTestTreeView', () => {
		refresf_test_explorer(testProv, context);
	});
	vscode.commands.registerCommand('bdd.create_step', () => {
		create_step();
	});
	vscode.commands.registerCommand('bdd.delete_pytest_cache', () => {
		bdd_funcs.delete_pytest_cache();
	});
	vscode.commands.registerCommand('bdd.go2fileExplorer', (item: TestItem) => {
		vscode.commands.executeCommand("workbench.files.action.showActiveFileInExplorer");
		vscode.commands.executeCommand("workbench.files.action.focusFilesExplorer");
	});
	vscode.commands.registerCommand('bdd.go2testExplorer', (item) => {
		const path = item.path.replace('/c:', 'c:');
		context.workspaceState.update('last_selected_path', path);
		testProv.last_path = path;
		testProv.refreshItem(undefined);
		vscode.commands.executeCommand('workbench.view.extension.test');
	});
	
	// Completions Provider
	const complsProv =vscode.languages.registerCompletionItemProvider('feature', {
		provideCompletionItems(document: vscode.TextDocument, position: vscode.Position,
							   token: vscode.CancellationToken, context: vscode.CompletionContext){
			var starting = document.lineAt(position.line).text
				.slice(0, position.character-1).trim();
			var items =  bdd_funcs.get_completions(searchDecs, starting);
			return items;
		}
	});

	context.subscriptions.push(complsProv);

	// Scemantic Provider
	const scemProv = vscode.languages.registerDocumentSemanticTokensProvider(
		{ language: 'feature', scheme: 'file'}, new DocumentSemanticTokensProvider(), legend);
	context.subscriptions.push(scemProv);

	// Test Explorer	
	init_test_explorer(testProv, context, true);

	vscode.window.registerTreeDataProvider('bdd_refs', refsProv);
	refsProv.view = vscode.window.createTreeView("bdd_refs", {treeDataProvider: refsProv, showCollapseAll: true});

	test_function();
}

function init_refs_explorer(){
	refsProv.coms.forEach(element => { element.dispose();});
	
	refsProv = new RefItemProvider();
	vscode.window.registerTreeDataProvider('bdd_refs', refsProv);
	refsProv.view = vscode.window.createTreeView("bdd_refs", {treeDataProvider: refsProv, showCollapseAll: true});

	// Refs Explorer TreeView Commands
	refsProv.coms.push(vscode.commands.registerCommand('bdd.go2Reference', (item) => {
		refsProv.go2Reference(item);
	}));

}
function refresf_test_explorer(testProv: TestItemProvider, 
							   context: vscode.ExtensionContext){
	
	var prom = testProv.collect_all_tests_async().then( text =>{
		vscode.window.showInformationMessage("Pytest-BDD: Test Discovery Completed!");
		testProv.coollected_tests = text;
		testProv.clear_results();
		testProv.refreshItem(undefined);
		context.workspaceState.update('collected_tests', text);
	});
	vscode.window.setStatusBarMessage('Pytest-BDD: Refreshing Test Explorer...', prom);
	
}
function init_test_explorer(testProv: TestItemProvider, 
							context: vscode.ExtensionContext,
							load_collected_tests: boolean = false ){
	testProv.coms.forEach(element => {element.dispose();});
	testProv.view?.dispose();

	var path: string|undefined = context.workspaceState.get('last_selected_path');
	if (path !== undefined){
		testProv.last_path = path;
	}

	testProv.coollected_tests = '';
	if (load_collected_tests){
		var collected_tests: string|undefined = context.workspaceState.get('collected_tests');
		if (collected_tests !== undefined){
			testProv.coollected_tests = collected_tests;
		}
	}

	vscode.window.registerTreeDataProvider('pytest', testProv);
	testProv.view = vscode.window.createTreeView("pytest", {treeDataProvider: testProv, showCollapseAll: true});
	
	testProv.action = actionTypes.idle;
	testProv.collect_all_tests_async().then( text =>{
		context.workspaceState.update('collected_tests', text);
		testProv.coollected_tests = text;
		testProv.refreshItem(undefined);
	});
	
	// Test Explorer TreeView Commands
	testProv.coms.push(vscode.commands.registerCommand('bdd.runItem', (item) => {
		if(testProv.action !== actionTypes.idle){return;}
		testProv.action = actionTypes.busy;
		testProv.view?.reveal(item, {expand:3}).then(x=>{
			testProv.run_explorer_test_sync(item);
		});
	}));
	testProv.coms.push(vscode.commands.registerCommand('bdd.debugItem', (item) => {
		testProv.debug_explorer_test(item);
	}));
	testProv.coms.push(vscode.commands.registerCommand('bdd.go2source', (item) => {
		testProv.go_to_source(item);
	}));

	testProv.coms.push(vscode.commands.registerCommand('bdd.stopDebuging', () => {
		testProv.stopDebuging();
	}));
	testProv.coms.push(vscode.commands.registerCommand('bdd.expandAll', () => {
		testProv.expandAll();
	}));

	testProv.coms.push(vscode.commands.registerCommand('bdd.onlyBdd', () => {
		testProv.switch_bdd_filter();
	}));

	testProv.coms.push(vscode.commands.registerCommand('bdd.refreshTestItem', (item) => {
		testProv.refreshItem(item)
	}));

	testProv.coms.push(vscode.commands.registerCommand('bdd.item_selected', (item: TestItem) => {
		testProv.item_selected(item);
		context.workspaceState.update('last_selected_path', item.path);
	}));
}