import * as vscode from 'vscode';
import { isUndefined } from 'util';
import * as bdd_funcs from './bdd_funcs';
import {testProv} from './extension';

export function create_step(){
    var ed = vscode.window.activeTextEditor;
    var line = ed?.selection.start.line;

    if(isUndefined(ed) || isUndefined(line)){return;}
    var bdd_text = ed?.document.lineAt(line).text.trim();
    var bdd_next_line = "";
    if (ed.document.lineCount > line + 1){
        bdd_next_line = ed?.document.lineAt(line + 1).text.trim();
    }
    var path = ed.document.uri.path;

    if (!bdd_text.startsWith("Then")
        && !bdd_text.startsWith("When")
        && !bdd_text.startsWith("Given")
        && !bdd_text.startsWith("And")){return;}
    var params = bdd_text.match(/"[\w]+"/g);

    var parser = vscode.workspace.getConfiguration().get("PytestBdd.parser");
    const parser_text = "(" + parser + "(\'";
    bdd_text = bdd_text.replace(/^Then /, "@then" + parser_text)
               .replace(/^When /, "@when" + parser_text)
               .replace(/^Given /, "@given" + parser_text)
               .replace(/^And /, "@replace_this" + parser_text);

    params?.forEach(param =>{
        var val = param.replace('"', '\{').replace('"', '\}');
        bdd_text = bdd_text.replace(param, val);
    });
    
    if (bdd_next_line.indexOf('|') >= 0 && parser === "Parser") {
        bdd_text += '\\n{table:T}';
    }

    bdd_text += '\'))\n';
    bdd_text += 'def step_function(';
    if (parser === "Parser") {
        bdd_text += 'session: Session';
    }

    params?.forEach(param =>{
         bdd_text += ', ' + param.replace(/"/g, "");
    });

    if (bdd_next_line.indexOf('|') >= 0 && parser === "Parser"){
        bdd_text += ', table';
    }

    bdd_text += '):\n';
    bdd_text += '    # Add Your Code Here\n';
    bdd_text += '    pass';


    path =  path.replace(/features\/[\w]*\.feature/, "step_defs") + "/";
    var stepsUri =  vscode.Uri.parse(path);
    const options: vscode.OpenDialogOptions = {
        defaultUri:stepsUri,
        canSelectMany: false,
        openLabel: 'Open',
        filters: {'Python files': ['py'],}
    };
    var rootDir = vscode.workspace.rootPath?.replace(/\\/g, '/');
    if(isUndefined(rootDir)){return;}
    var pattern = "**" + path.replace(rootDir, '').replace(/\/\//g, '/') + '*.py';
    vscode.workspace.findFiles(pattern).then( files =>{
        var count = 0;
        var fileUri;
        files.forEach(file=>{
            var name = file.path.split('/')[file.path.split('/').length-1];
            if (name !== '__init__.py'){
                count += 1;
                fileUri = file;
            }
        });
        if (count === 1){
            if(isUndefined(fileUri)){return;}
            vscode.window.showTextDocument(fileUri, 2).then(te =>{
                te.edit(tee =>{
                    const line_count = te.document?.lineCount;
                    if(isUndefined(line_count)){return;}
                    var p = new vscode.Position(line_count, 0);
                    tee.insert(p, "\n" + bdd_text);
                    bdd_funcs.select_focus_line(te, line_count+1);
                });
            });
        }else{
        vscode.window.showOpenDialog(options).then( fileUri =>{
            if(isUndefined(fileUri)){return;}
            vscode.window.showTextDocument(fileUri[0]).then(te =>{
                te.edit(tee =>{
                    const line_count = te.document?.lineCount;
                    if(isUndefined(line_count)){return;}
                    var p = new vscode.Position(line_count, 0);
                    tee.insert(p, "\n" + bdd_text);
                    bdd_funcs.select_focus_line(te, line_count+1);
                });
            });
        });
        }
    });
}