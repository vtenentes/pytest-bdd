<h1>
 PYTEST-BDD UNOFFICIAL README

 pytest-bdd extension
</h1>

<h2>Quick Start</h2>
<ul>
 <li> <h3> The extension is activated when a feature or python file is opened </h3></li> 
 <li> <h3> Find a sample repository <a href="https://gitlab.com/vtenentes/pytest-bdd/uploads/b8a35808c24907e984fee8aac050921f/sample_repo.zip" title="Sample Repo">here</a></h3></li> 
</ul>


<br>
<h2>Features</h2>
<ul>
  <li> <h3> Syntax Highlighting for feature files </h3></li> 
  <li> <h3> Auto Completions for BDD steps while editing a feature file </h3></li> 
  <li> <h3> Redirecting from the feature bdd step to the step definition function (right click, Bdd->Step Definition) by clicking ctr+shift+down </h3></li> 
  <li> <h3> Run any bdd scenario directly from the feature file (right click, Bdd-> Run Scenario) by clicking ctr+shift+r (also available with no bdd tests) </h3></li> 
  <li> <h3> Debug any scenario directly from the feature file (right click, Bdd-> Debug Scenario) by clicking ctr+shift+t (also available with no bdd tests) </h3></li> 
  <img src="https://gitlab.com/vtenentes/pytest-bdd/-/raw/master/media/demo.gif?raw=true" alt="Demo">

  <li> <h3> Integrated Test explorer: </h3></li> 
  <ul>
    <li> <h3> Test explorer: Run/Debug tests by selecting folders, python test files, feature files or single scenarios</h3></li>
    <li> <h3>Test explorer: Redirect from the test explorer item to the file</h3></li>
    <li> <h3>Test explorer: Check last output by clicking an item (not available for debugging)</h3></li>
    <li> <h3>Test explorer: Option to show only BDD scenarios</h3></li>
    <img src="https://gitlab.com/vtenentes/pytest-bdd/-/raw/master/media/demoExplorer.gif?raw=true" alt="Demo">
  </ul>

  <li> <h3>Find bdd step decorator references (right click,  Bdd-> Find References). Results are shown in 'BDD References' view container </h3></li> 

  <li> <h3> Delete Pytest Cache in workspace (Ctr+Shift+p, Bdd-> Delete Pytest Cache Files). This commands works only if the default terminal is "Windows PowerShell" </h3></li> 

  <li> <h3> Create Step Definition  (right click, Bdd-> Create Step).  <br> 
        Use the following setting to set your parser (default value is "Parser")  </h3>

     "PytestBdd.parser": "cfparse"
  <img src="https://gitlab.com/vtenentes/pytest-bdd/-/raw/master/media/demoCreateStepDef.gif?raw=true" alt="Demo">
  </li> 
</ul>

<br>
<h2> Requirements </h2>
<ul>
 <li> <h3> Python (ms-python.python) addon installed </h3></li> 
 <li> <h3> Disable any "gherkin", "cucumber" related addons </h3></li> 
 <li> <h3>  BDD tests should follow this structure: </h3></li> 


        rootdir\
                example1\
                        features\
                                my_feature_1.feature
                                my_feature_2.feature
                        step_defs\
                                my_steps_1_steps.py (must end with '_steps.py')
                                my_steps_2_steps.py
                        test_run_my_features.py (must start with 'test_' and to include the features, scenarios("./features/"))
                        __init__.py
 <li> <h3>  Restart vscode after installing this extension </h3></li> 
</ul>

<br>
<h2> Extension Settings </h2>
<ul>
 <li> <h3> Add the following setting to your json settings file: </h3></li> 

        "editor.semanticTokenColorCustomizations": {
            "enabled": true, // enable for all themes
        },

 </ul>

<br>
<h2> Themes tested </h2>
<ul>
 <li> <h3> Darcula Pycharm with Light GUI </h3></li> 
 <li> <h3> Darcula Pycharm with Dark GUI </h3></li> 
 <li> <h3> Light (Visual Studio) </h3></li> 
 <li> <h3> Light+ (default light) </h3></li> 
 <li> <h3> Dark (Visual Studio) </h3></li> 
 <li> <h3> Dark+ (default dark) </h3></li> 


 </ul>

<br>
<h2> Enjoy! </h2>

