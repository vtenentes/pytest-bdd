import { isNull, isUndefined} from "util";
import * as lib from './mylib';
import { dir } from 'console';
import { resolve } from "path";
import * as vscode from 'vscode';
import * as bdd_funcs from './bdd_funcs';

export function collect_test_list(path: string|undefined|null): string[] {
    if(path ===undefined || path === null){ return [];}
    var text = collect_tests(path);
    var regs = text.match(/<Function\s\w.*>/g);
    var tests: string[] = [];
    regs?.forEach(reg => {
        tests.push(reg.replace(/<Function\s/,'').replace('>', ''));
    });
    return tests;
}

export function collect_test_list_async(path: string|undefined|null, mark :string =""): Thenable<string[]> {
    if(path ===undefined || path === null){ return Promise.resolve([]);}
    return collect_tests_async(path, mark).then(text=>{
        var regs = text.match(/<Function\s[\w|\[|\]]*>/g);
        var tests: string[] = [];
        regs?.forEach(reg => {
            tests.push(reg.replace(/<Function\s/,'').replace('>', ''));
        });
        return tests;
    });

}

export function collect_tests(path: string, mark: string =""): string {
    var root = vscode.workspace.rootPath;
    var com = 'pytest  -W ignore  --collect-only  --continue-on-collection-errors "' + path + '" --rootdir="' + root + '"';
    if (mark.length > 0){
        com += " -m " + '"' + mark + '"';
    }
    console.log("collect_tests: " + path)
    return lib.exec_command_sync(com);
}

export function collect_tests_async(path: string, mark: string =""): Thenable<string> {
    var root = vscode.workspace.rootPath;
    var com = "pytest -W ignore --collect-only --continue-on-collection-errors " + path + ' --rootdir="' + root + '"';
    if (mark.length > 0){
        com += " -m " + '"' + mark + '"';
    }
    console.log("collect_tests_async: " + path)
    return lib.exec_com_async(com);
}

export function scenarios_in_feature_dir(path: string): Scenario[]{
    var scens: Scenario[] = [];
    var files = features_in_dir(path);
    files.forEach(file =>{
        scens = scens.concat(scenarios_in_feature(file));
    });
    return scens;
}

export function scenarios_in_feature(path: string|null, specificLine: number|null = null): Scenario[]{
        if(path == null){ return [];}
    var scens: Scenario[] = [];
    var text = lib.read_file(path);
    var tests = text.match(/(Scenario:|Scenario Outline:).*/g);
    tests?.forEach(test=>{
        var line = text.slice(0, text.indexOf(test)).match(/\n/g)?.length;
        if(isUndefined(line)){line = 0;}
        var label = test.replace(/Scenario:/, '')
                        .replace(/Scenario Outline:/, '').trim();
        if (isUndefined(line)){return;}
        var scen = new Scenario(line, label, path);
        
        if (specificLine != null){
            if (scen.line == specificLine){
                scens.push(scen);
            }
        } else{
            scens.push(scen);
        }
       
    });
    return scens.sort((a,b)=> (a.label > b.label)?1:(b.label> a.label)?-1:0);
}

export function scenarios_in_python(path: string|undefined): Scenario[] {
    if(isUndefined(path)){ return [];}
    var tests: Scenario[] = [];
    var text = lib.read_file(path);
    var regs = text.match(/def\s{1,}test\w*/g);
    regs?.forEach(t=>{
        var line = text.slice(0, text.indexOf(t)).match(/\n/g)?.length;
        var label = t.replace(/def\s{1,}/, '');
        if (isUndefined(line)){return [];}
        var scen = new Scenario(line, label);
        tests.push(scen);
        });
    return tests;
}

export function single_features_in_python(path: string|undefined): Feature[] {
    if(path === undefined){ return [];}
    var text = lib.read_file(path);
    var features: Feature[] = [];
    // Check For scenarios importing features
    // scenarios("./features/")
    var rx = /scenarios\s*\(\s*['|"].*['|"].*\)/g;
    var regs = text.match(rx); 
    regs?.forEach(reg => {
        var regarr = reg.match(/["|'].*["|']./);
        if(regarr == null){return;}
        var rel_path = Array.from(regarr)[0];
        if(rel_path.indexOf('.feature')>=0) {
            var feature_name = rel_path.split('/')[rel_path.split('/').length-1];
            feature_name = feature_name.replace('"', '').replace(',', '')
                                       .replace(')', '').replace(/'/g, '');
            var rel_path = Array.from(regarr)[0];
            var scenarioPath = path.slice(0,path.lastIndexOf('/'));
            rel_path.match(/\.\.\//g)?.forEach( rep => {
                scenarioPath = scenarioPath.slice(0,scenarioPath.lastIndexOf('/'));
            });

            var pathMatch =  rel_path.match(/[\/]*[^\.].*\//);
            if (pathMatch == null){return;}
            var pathToAppend = pathMatch[0].replace(/'/g,'').replace('../','').replace('./','');
            if (scenarioPath.endsWith('/') || pathToAppend.startsWith('/')){
                scenarioPath += pathToAppend;
            }
            else{
                scenarioPath += '/' + pathToAppend;
            }

            var f = new Feature(feature_name, scenarioPath + feature_name, path);
            if(!feature_exist(f.path, features)){
                features.push(f);
            }
        }
    });

    rx = /@scenario\s*\(\s*['|"].*['|"]\s*,\s*[?(\r\n)]*\s*['|"].*['|"].*[?(\r\n)]*.*\).*\r\n.*def\s*test\w*\(/g;

    regs = text.match(rx); 
    regs?.forEach(reg => {
        var regarr = reg.match(/["|'].[^'"]*["|']/g);
        if(regarr == null){return;}
        var rel_path = Array.from(regarr)[0].replace(/'/g,'').replace(/"/g,'');
        var feature_name = Array.from(regarr)[0].replace(/'/g,'').replace(/"/g,'');
        if(feature_name.indexOf('/')>=0) {
            feature_name = feature_name.split('/')[feature_name.split('/').length - 1];
        }

        if(rel_path.indexOf('.feature')>=0) {
            var scenarioPath = path.slice(0,path.lastIndexOf('/'));
            rel_path.match(/\.\.\//g)?.forEach( rep => {
                scenarioPath = scenarioPath.slice(0,scenarioPath.lastIndexOf('/'));
            });
            var pathMatch =  rel_path.match(/[^\.].*\//);
            if (pathMatch == null){return;}

            var pathToAppend = pathMatch[0].replace(/'/g,'').replace('../','').replace('./','');
            if (scenarioPath.endsWith('/') || pathToAppend.startsWith('/')){
                scenarioPath += pathToAppend;
            }
            else{
                scenarioPath += '/' + pathToAppend;
            }

            var f = new Feature(feature_name, scenarioPath + feature_name, path);
            if(!feature_exist(f.path, features)){
                features.push(f);
            }
        }
    });
   return features.sort((a,b)=> (a.name > b.name)?1:(b.name> a.name)?-1:0);
    
}

function feature_exist(path: string, features: Feature[]){
        for (var i=0; i<features.length; i++){
            if (features[i].path == path){
                return true;
            }
        }
        return false;
}

export function features_in_python(path: string|undefined): Feature[] {

    if(path === undefined){ return [];}
    var features: Feature[] = [];
    var dirs = feature_dirs_in_python(path);
    dirs.forEach(dir =>{
        var features_in_folder = features_in_dir(dir);
        features_in_folder.forEach(feature=>{
            var f = new Feature(feature, dir + feature, path);
            features.push(f);
        });
        
    });

    var singles = single_features_in_python(path);
    features = features.concat(singles);
    return features.sort((a,b)=> (a.name > b.name)?1:(b.name> a.name)?-1:0);
}

export function feature_dirs_in_python(path: string|undefined): string[] {
    if(path == undefined){ return [];}
    var dirs: string[] = [];
    var text = lib.read_file(path);
    // Check For scenarios importing features
    // scenarios("./features/")
    const rx = /scenarios\s*\(\s*['|"].*['|"].*\)/g;
    var regs = text.match(rx); 
    regs?.forEach(reg => {
        var regarr = reg.match(/["|'].*["|']./);
        if(isNull(regarr)){return;}
        var rel_path = Array.from(regarr)[0];
        if(rel_path.indexOf('.feature')>=0) {return;}
        var scenarioPath = path.slice(0,path.lastIndexOf('/'));
        rel_path.match(/\.\.\//g)?.forEach( rep => {
            scenarioPath = scenarioPath.slice(0,scenarioPath.lastIndexOf('/'));
        });
        var pathToAppend =  rel_path.match(/\/[^\.].*\//);
        scenarioPath += pathToAppend;
        dirs.push(scenarioPath);
    });
    
    return dirs;
}

export function duplicates_in_python(path: string|null): Duplicate[] {
    if (isNull(path)){return [];}
    var dupls: Duplicate[] = [];
    var text = lib.read_file(path);
    // Check For @scenario decorator importing scenario
    // @scenario('./features/example.feature', 'My 3rd Scenario')
    // def test_function():
    const rx = /@scenario\s*\(\s*['|"].*['|"]\s*,\s*[?(\r\n)]*\s*['|"].*['|"].*[?(\r\n)]*.*\).*\r\n.*def\s*test\w*\(/g;
    var regs = text.match(rx); 
    
    regs?.forEach(reg => {
        try{
        var featurePath = path.slice(0, path.lastIndexOf('/'));
        var regarr = reg.match(/["|'].[^,]*["|']/g);
        if(regarr == null || regarr == undefined 
            ||  Array.from(regarr)[0].split(',')[0] == undefined
            ||  Array.from(regarr)[1].split(',')[0] == undefined)
            {return;}
        var rel_path = Array.from(regarr)[0].split(',')[0].replace(/['|"]/g, '').trim();
        var oldName = Array.from(regarr)[1].trim();
        oldName = lib.strim(oldName, '"');
        oldName = lib.strim(oldName, '\'');
        regarr = reg.match(/def\s{1,}\w*\s*\(/g);
        if(regarr == undefined){return;}
        var newName = regarr?.toString().replace('def', '').replace('(', '').trim();
    
        rel_path.match(/\.\.\//g)?.forEach( rep => {
            featurePath = featurePath.slice(0,featurePath.lastIndexOf('/'));
        });

        var pathToAppend =  rel_path.match(/\/[^\.].*/);
        featurePath += pathToAppend;
        if(newName == undefined){return;}
        var d = new Duplicate(newName, oldName, featurePath);
        dupls.push(d);
        } catch{}
    });
    return dupls;
}

export function features_in_dir(path: string): string[]{
     return lib.items_in_dir(path, /.*\.feature$/g);
}

export function pythons_in_dir(path: string|undefined): string[]{
    if(isUndefined(path)){ return [];}
    if(path.indexOf('/') !== path.length-1){path += '/';}
    var items = lib.items_in_dir(path, /.*\.py$/g);
    var keys = ['__init__.py'];
    items = lib.list_remove(items, keys);
    return items;
}

export function folders_in_dir(path: string|undefined): string[]{
    if(isUndefined(path)){ return [];}
    if(path.indexOf('/') !== path.length-1){path += '/';}
    var items = lib.items_in_dir(path, /^[\w ]*$/g);
    var keys = ['__pycache__', ];
    items = lib.list_remove(items, keys);
    return items;
}


export function dirs_and_pythons_in_dir(path: string|undefined): string[]{
    if(isUndefined(path)){ return [];}
    var items: string[] = [];
    items = items.concat(folders_in_dir(path));
    items = items.concat(pythons_in_dir(path));
    return items;
}

export function is_any_feature_in_subdirs(paths: string[]): boolean{
    return lib.is_any_item_in_subdirs(paths, /^\w*\.feature$/g);
}

export function is_any_python_in_subdirs(paths: string[]): boolean{
    return lib.is_any_item_in_subdirs(paths, /^\w*\.py$/g);
}

export function is_any_python_test_in_subdirs(paths: string[]): boolean{
    return lib.is_any_item_in_subdirs(paths, /^test\w*\.py$/g);
}

export function test_function(){

    // var root = vscode.workspace.rootPath;
    // var aaa = collect_test_list(root);
    var paths = ['C:/Users/vtenente/Downloads/bdd_tests/'];
    // var resp = lib.is_any_feature_in_subdirs( paths);
    var p = 'C:/Users/vtenente/Downloads/bdd_tests/sample_1/test_example.py';
    var pd = 'C:/Users/vtenente/Downloads/bdd_tests/sample_1';
    var f = 'C:/Users/vtenente/Downloads/bdd_tests/sample_1/features/example.feature';
    var fd = 'C:/Users/vtenente/Downloads/bdd_tests/sample_1/features/';
    // var xxxx = lib.features_in_dir('C:/Users/vtenente/Downloads/bdd_tests/sample_1/features');
    // var xxx2 = lib.python_in_dir('C:/Users/vtenente/Downloads/bdd_tests/sample_1/');

    // var xx1 = duplicates_in_python(p);
    // var xx2 = scenarios_in_python(p);
    // var xx3 = scenarios_in_feature(f);
    // var xx4 = collect_tests(p);
    // var xx5 = collect_test_list(p);
    // var xx6 = scenarios_in_feature_dir(fd);
    // var xx7 = feature_dirs_in_python(p);
    // var xx8 = features_in_dir(fd);
    // var xx9 = pythons_in_dir(pd);
    // var xxx10 = folders_in_dir(pd);
    // var xxx11 = dirs_and_pythons_in_dir(pd);
    // var xxx12 = is_any_feature_in_subdirs([pd]);
    // var xxx13 = is_any_python_in_subdirs([fd]);
    // var xxx14 = collect_tests_async(root).then(x=>{
    //     var y = 1;
    // });
    // p = 'c:/Repos/Server/OPS-10906/test/uctest/uctest/features/account/general/test_account_restriction_reason.py';
    // var feats = ;
    // console.log(feats);
    // console.log(xx5);

}

export class Duplicate {
        constructor(
        public testName: string,
        public scenarioName: string,
        public feature: string
    ){
    }
}  

export class Python {
	
	public features: Feature [] = [];
	public py_tests: string [] = [];
	public file_tests: Scenario []= [];
	constructor(public path: string)
	{

	}
}

export class Feature {
    constructor(
        public name: string,
        public path: string,
        public pythonPath: string,
    ){}
}

export class Scenario {

    constructor(
        public line: number,
        public label: string,
        public feature: string | null = null,
        public name : string | null = null,
    ){
        if(isNull(this.name)){
            this.name = bdd_funcs.format_test_name(this.label, 
                                                    false, 
                                                    this.feature !== null);
        }

    }
}
