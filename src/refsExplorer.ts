import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as glob from 'glob';
import { isUndefined } from 'util';
import { promises } from 'dns';
import * as bdd_funcs from './bdd_funcs';
import { rootCertificates } from 'tls';

export class RefItemProvider implements vscode.TreeDataProvider<Reftem> {
	private _items: Reftem[] = [];
	private _onDidChangeTreeData: vscode.EventEmitter<Reftem | undefined> = new vscode.EventEmitter<Reftem | undefined>();
	readonly onDidChangeTreeData: vscode.Event<Reftem | undefined> = this._onDidChangeTreeData.event;
	public view: vscode.TreeView<Reftem> |undefined;
    public coms: vscode.Disposable[] =[];
    public results: bdd_funcs.Record[] = [];
	constructor() {
	}

	getParent(element: Reftem){
		return element;
	}

	refresh(){

	}

	public go2Reference(item: Reftem){
		bdd_funcs.show_doc(item.record.file.fsPath).then( te => {
			bdd_funcs.select_focus_line(te, item.record.line, 2);
		});
    }
	refreshItem(item: Reftem): void {
		this._onDidChangeTreeData.fire(item);
	}

	getTreeItem(element: Reftem): vscode.TreeItem {
		return element;
	}

	getChildren(element?: Reftem): Thenable<Reftem[]> {
		return Promise.resolve(this.getRefItems(element));
	}

	private getRefItems(element?: Reftem): Thenable<Reftem[]> {
		var childs: Reftem[] = [];
		if(element == undefined){
			this.results.forEach(rec => {
				var item = new Reftem(rec.text, 2, rec, itemTypes.step);
				this._items.push(item);
				childs.push(item);
			});
		} else if (element.type === itemTypes.step){
			var label = element.record.file.fsPath;
			var root = vscode.workspace.rootPath?vscode.workspace.rootPath:'';
			label = label.replace(root, '..');
			label = "Path: " + label.replace('..\\uctest\\features', '..');
			var feature = label.split('\\').reverse()[0];
			var item = new Reftem(label.replace(feature, ''), 0, element.record, itemTypes.relPath);
			childs.push(item);
			label = "File: " + feature + "  Line: " + element.record.line;
			item = new Reftem(label, 0, element.record, itemTypes.relPath);
			childs.push(item);
		}
		return Promise.resolve(childs);
	}

	private pathExists(p: string): boolean {
		try {
			fs.accessSync(p);
		} catch (err) {
			return false;
		}

		return true;
	}
}

export class Reftem extends vscode.TreeItem {

	constructor(
		public label: string,
		public collapsibleState: vscode.TreeItemCollapsibleState,
		public record: bdd_funcs.Record,
		public type: itemTypes,
		public contextValue?: string,
		public fpath?: string
	) {
		super(label, collapsibleState);

	}

	// command = {
	// 		command: "bdd.item_selected",
	// 		title: "Select Item",
	// 		arguments: [this]
	// 	};
	

	get tooltip(): string {
		return ``;
	}

	get description(): string {
		return '';
	}

	get icon():string{
		var svg = '';
		if(this.type === itemTypes.step){
			svg = 'reference.svg';
		}
		return svg;
	}

	get iconPath(){
		var iconPath = {
			light: path.join(__filename, '..', '..', 'resources', 'light', this.icon),
			dark: path.join(__filename, '..', '..', 'resources', 'dark', this.icon)
		};
		return iconPath;
	}
}

enum itemTypes {
	step,
	relPath,
	none,
}