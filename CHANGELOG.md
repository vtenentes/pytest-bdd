# Change Log
All notable changes to the "Pytest-BDD" extension will be documented in this file.

## Version 0.3.18
###  New Features
        Add ability to find feature files in subdirectories of the `features` directory.

## Version 0.3.17
###  Bug Fixes
	Allow Scenario name to include apostrophes (')

## Version 0.3.16
### New Features
	Updated an import to resolve an issue on file systems with case sensitivity

## Version 0.3.15
### New Features
	Updated the search of step definitions to include subfolders. This allows users to jump to definitions from a feature file to any step defined in a subfolder of the step_defs folder.

## Version 0.3.14
### New Features
	Test explorer - Show tests in features even if they are declared in a test pyton file (BDD)

## Version 0.3.13
### New Features
	References for pytest markers	

## Version 0.3.12
###  Bug Fixes
	Test explorer - fix for parsing scenarios("./features/single.feature")

## Version 0.3.11
	Add a sample repository to the readme file

## Version 0.3.10
###  Bug Fixes
	Test explorer - fix icons
	
## Version 0.3.9
###  Bug Fixes
	Test explorer - paths with spaces - fix for test explorer

## Version 0.3.8
###  Bug Fixes
	Test explorer - paths with spaces

## Version 0.3.7
###  Bug Fixes
	Test explorer - tests not showing

## Version 0.3.6
### New Features
	Run File
	Debug File 
###  Bug Fixes
	Test explorer - pytest collection paths

## Version 0.3.5
### New Features
	Improved Test Exploring 

## Version 0.3.4
###  Bug Fixes
	Test explorer refresh bugs

## Version 0.3.3
### New Features
    Open test file in Explorer (folder icon in test explorer)
###  Bug Fixes
	Refresh test explorer did not incude new testss

## Version 0.3.2
### New Features
    Use Scenario name for labels in Test Explorer
###  Bug Fixes
	Use more special characters in Scenario name

## Version 0.3.1
###  Bug Fixes
	Use of	;,',",’,& in Scenario name

## Version 0.3.0
###  Bug Fixes
- Test Explorer, navigate on item selection
- Test Explorer, item output configuration
- Similar Feature test items 

## Version 0.2.9
###  Bug Fixes
- Collect tests on refresh command

## Version 0.2.8
###  Bug Fixes
- Incorrect parametrized scenarios in features name

## Version 0.2.7
###  Bug Fixes
- add rootdir to test discovery

## Version 0.2.6
###  Bug Fixes
- Incorrect parametrized scenarios name
- go2source parametrized scenarios 

## Version 0.2.5
###  Bug Fixes
- Run Scenario fixed

## Version 0.2.4
###  Bug Fixes
- Scenario Outline Highlighting using <> notation

## Version 0.2.3
### New Features
- Test Explorer redesigned
- Delete Pytest Cache Command 


## Version 0.2.2
### Bug Fixes
- minor fixes regarding file structure




## Version 0.2.1
### New Features
- Create Step Definition  

## Version 0.2.0
### New Features
- BDD Commands "run scenario", "debug scenario", "go to step definition" added to right click menu

## Version 0.1.9
### New Features
- Support all pytest-bdd parsers

## Version 0.1.8
### New Features
- Terminal scrolls to bottom while runnning a test
- Show test path in output window while running a test from test explorer
- Find bdd decorator References for two-line decorators
- Redirecting from the feature bdd step to the step definition function for two-line decorators 
- New view Container "BDD References"

## Version 0.1.7
### Bug Fixes
- Find Bdd decorator References for "And" steps 

## Version 0.1.6
### New Features
- Gherkin Autocomplete Snippets Improved
- Find Bdd decorator References (right click : Bdd: Find References). Results are shown in test view
### Bug Fixes
- test names incuding characters [',', '.'] are failing
- if bdd-output terminal is closed, will not reopen automatically
- wrong redirection for tests with similar names 
- common tests (no BDD) key combinations (ctr+shift+r/t) 

## Version 0.1.5
- Initial release
