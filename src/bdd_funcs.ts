import * as vscode from 'vscode';
import {isUndefined, isNull} from 'util';
import {testProv} from './extension';
import { off } from 'process';
import * as pytest from './pytest';

export var output = vscode.window.createOutputChannel('bdd-output (run)');
export var term = vscode.window.createTerminal('bdd-output');
export var pytest_com = 'pytest -q ';
export const log_file: string = "pytest_bdd_logs.log";

export class Record {
	text = '';
	file: vscode.Uri = vscode.Uri.prototype;
	line: number = -1;
}

export class Search {
	step_decs: Array<Record> = [];
	step_files: Array<vscode.Uri> = [];
	constructor(decs: Record[], files: vscode.Uri[]) {
		this.step_decs = decs;
		this.step_files = files;
	}
}

export function exec_command(com: string){
	output.append(com);
	var exec = require('child_process').exec;
	exec(com, function(error: string, stdout: string, stderr: string) {
	// command output is in stdout
	output.append(stdout);
	output.show();
	});
}

export function find_mark_references(mark: string) {
	var results: Record[] = [];
	var text = testProv.collect_all_tests(mark).split('warnings summary')[0];

	return vscode.workspace.findFiles("**/{*.feature,test_*.py}").then((files ) => {
		files.forEach((file) => {
			
			var scs: pytest.Scenario[] =  [];
			var path = file.path.replace('/c:/', 'C:/');
			if (path.includes('.feature')){
				scs = pytest.scenarios_in_feature(path);
				for(var i =0; i<scs.length; i++){ 
					scs[i].name = 'test_' + scs[i].name;
				}
			} else if (text.includes(path.split('/')[path.split('/').length-1])){
				scs = pytest.scenarios_in_python(path);
			}

			scs.forEach((scen) => {
				if ( scen.name != null && text.includes('<Function ' + scen.name) ){
					var result = new Record()
					result.file = vscode.Uri.file(path);
					result.line = scen.line
					result.text = scen.label
					results.push(result)
				}
			});
		});
		vscode.commands.executeCommand("workbench.view.extension.bdd_references_container");
		return results;
	});
}

export function find_references(){
	var line = vscode.window.activeTextEditor?.selection.active.line;
	if ( line === undefined) {return Promise.resolve([]);}
	if (vscode.window.activeTextEditor?.selection.active.character === 0) {
		line -= 1;
	}
	var doc = vscode.window.activeTextEditor?.document;
	if ( doc === undefined) {return Promise.resolve([]);}
	var dec = vscode.window.activeTextEditor?.document.lineAt(line).text;
	var start  = vscode.window.activeTextEditor?.selection.start.character;
	var end = vscode.window.activeTextEditor?.selection.end.character;
	if (start == undefined || end == undefined || dec == undefined) {return Promise.resolve([]);}

	var final_start=0, final_end=end;
	for (var i=start; i >= 0; i--){
		if (dec[i] == '@' ){
			final_start = i
			break;
		}
	}
	for (var i=end; i < dec.length; i++){
		if (dec[i] == ' ' || i == dec.length-1){
			final_end = i
			break;
		}
	}
	var text = dec;
	if (text === undefined) {return Promise.resolve([]);}

	var isMark = text.indexOf('(') < 0 
		&& text.length > 0 
		&& text.trim()[0] == '@' 
		// && doc.fileName.toLocaleLowerCase().endsWith('.feature')

	if (isMark){ 
		// @pytest.mark.FOO 
		text = text.substring(final_start, final_end + 1 )
		text = text.replace('@', '').trim();
		if (!doc.fileName.toLocaleLowerCase().endsWith('.feature')){
			text = text.replace('pytest.mark.', '').trim();
		}
	} else if(text.indexOf(')') < 0){
		// Step Function decorator
		// @when(Parser('{player_name} is a player'))
		text = text.replace('+', '');
		text = text.replace('\\', '');
		text += doc.lineAt(line + 1).text;
		text = format_decorator(text);
	}else if(text.indexOf(')') > 0){
		text = format_decorator(text);
	}
	dec = text;
	dec.replace('{', "\"").replace('}', "\"");

	if (isMark){ 
		return find_mark_references(dec);
	}

	return search_feature_files_for_refs().then( (recs)=> {

		var results: Record[] = [];
		if (dec == undefined){return results;}

		// Step Function decorator
		var rec_from_dec = new Record();
		rec_from_dec.text = dec;
		
		recs.forEach(rec =>{
			if(record_matches_step(rec_from_dec, rec.text))	{
				results.push(rec);
			}
		});
		// "workbench.actions.treeView.pytest.refresh"
		vscode.commands.executeCommand("workbench.view.extension.bdd_references_container");
		return results;
	});
}

export function debug_file(){
	run_file(true);
}

export function debug_scenario(){
	run_scenario(true);
}

export function run_file(debug=false){
	var filename = vscode.window.activeTextEditor?.document.uri.path;
	if ( filename != undefined){
		run_or_debug(filename, debug);
	}
}


export function run_scenario(debug=false){
	// Get step text from the active text editor
	var line_num = vscode.window.activeTextEditor?.selection.start.line;
	var filename = vscode.window.activeTextEditor?.document.uri.path;
	if (line_num == undefined){return;} 
	
	do {
		
		var step = vscode.window.activeTextEditor?.document.lineAt(line_num);
		if (step == undefined){	return;	}
		line_num -= 1;
	} while( line_num >=0 && step?.text.indexOf('Scenario') < 0);
	// Go to step definition

	if (step?.text.indexOf(':') >= 0 && step?.text.trim().startsWith('Scenario')){
		var step_label = step.text.replace('Scenario:', '').replace(/Scenario\s* Outline\s*:/, '').trim();
		var step_str = format_test_name(step_label);
		
		if (filename === undefined){return;}
		var sub_dir = filename.split('/')[filename.split('/').length-3];
		sub_dir = '**/' + sub_dir + '/test_*.py';
		vscode.workspace.findFiles(sub_dir).then((files, ) => {
			files.forEach((file) => {
				var features =	pytest.features_in_python(file.path.replace('/c:', 'c:'));

				var doc = get_doc_from_path(file.path);
				doc.then(td =>{
					const text = td.getText();
					var found = false;
					for (let index = 0; index < features.length; index++) {
						const f = features[index];
						if (filename === undefined){return;}
						if(filename.replace('/c:', 'c:') === f.path.replace('/c:', 'c:'))
						{
							found = true;
						}

						if (found && text.indexOf(step_label)<=0){
							var label_exec = 'test_' + step_str;
							// Replace test name if @scenario decorator is used
							var comm = file.fsPath + '::' + label_exec ;
							run_or_debug(comm, debug);
							break;
						}

						if (found && text.indexOf(step_label)>=0){
							var dups = pytest.duplicates_in_python(file.path.replace('/c:', 'c:'));
							for (let index = 0; index < dups.length; index++) {
								const dup = dups[index];
								if (step_label === dup.scenarioName){
									var label_exec = dup.testName;
									var comm = file.fsPath + '::' + label_exec ;
									run_or_debug(comm, debug);
									break;
								}
							}
							break;
						}
					}
				});
			});
		});
		return;
	}else{
		var step_str = get_function_above();
		var comm = filename + '::' + step_str ;
		run_or_debug(comm, debug);
	}
}

export function get_function_above(): string {
	var line_num = vscode.window.activeTextEditor?.selection.start.line;
	if( line_num == undefined){return '';}
	do {
		var step = vscode.window.activeTextEditor?.document.lineAt(line_num);
		if (step == undefined){	return '';}
		line_num -= 1;
	} while( line_num >=0 && step?.text.indexOf('def ') < 0);

	var step_str = step.text.split('(')[0].replace('def', '').trim();
	return step_str;
}

function run_or_debug(test_path: string, debug: boolean){
	var root = vscode.workspace.rootPath;
	test_path = format_test_path(test_path);
	if(debug){
		testProv.debug_test(test_path);
	}else{
		// term.sendText('cd ' + root );
		term.sendText(pytest_com +'"'+ test_path + '"');
		term.show();
		vscode.commands.executeCommand("workbench.action.terminal.scrollToBottom");
	}
}

vscode.window.onDidCloseTerminal(closed_term =>{
	if(closed_term.name === term.name){
		term.dispose();
		term = vscode.window.createTerminal('bdd-output');
	}
});

export function format_test_path_name(comm: string): string{
	if(comm.split('::').length === 2){
		var path = comm.split('::')[0];
		var name = comm.split('::')[1];
		path = format_test_path(path);
		name = format_test_name(name);
		return path + '::' + name;
	}else{
		return comm;
	}
}

export function format_test_name(name: string, removeminus=true, lowercase=true): string{
	// Also modify class Scenario in pytest.ts
	if (removeminus){
		name = name.replace(/-/gi, '');
	}
	if (lowercase){
		name = name.toLowerCase();
	}

	return 	name.replace(/ /gi, '_').replace(/"/gi, '')
		.replace(/,/gi, '').replace(/\./gi, '')
		.replace(/;/gi, '').replace(/'/gi, '')
		.replace(/’/gi, '').replace(/&/gi, '')
		.replace(/=/gi, '').replace(/%/gi, '')
		.replace(/#/gi, '').replace(/@/gi, '')
		.replace(/!/gi, '').replace(/`/gi, '')
		.replace(/~/gi, '').replace(/:/gi, '')
		.replace(/{/gi, '').replace(/}/gi, '')
		.replace(/\(/gi, '').replace(/\)/gi, '')
		.replace(/\</gi, '').replace(/\>/gi, '')
		.replace(/\+/gi, '').replace(/\*/gi, '')
		.replace(/\^/gi, '').replace(/\$/gi, '')
		.replace(/\€/gi, '').replace(/\./gi, '')
		.replace(/\?/gi, '').replace(/\//gi, '')
		.replace(/-/gi, '');
}

export function format_test_path(path: string): string{
		var new_path = path;
		do{
			new_path = new_path.replace('\\', '/');
		}while( new_path.indexOf('\\') >=0);
	
		return new_path.replace('/c:/', 'C:/').replace('/C:/', 'C:/')
					   .replace('c:/', 'C:/');
}

export function format_decorator(text: string): string{
	var reg = text.match(/\([ |r]*['|"].*['|"][ ]*\)/);
	if(reg == null){ return '';}
	var body =  reg[0];
	body = body.replace(/\([ |r]*['|"]/, '');
	body = body.replace(/['|"][ ]*\)/, '');
	body = body.replace(/\\n[ ]*{.*:T}/, '');
	body = body.replace(/['|"][ ]*'/, '');
	body = body.replace(/;/g, '');

	reg = text.match(/@[a-z]*\(/);
	if(isNull(reg)){return '';}
	var keyw = reg[0].replace('@', '').replace('(', '').trim();

	var result =  keyw + ' ' + body;
	return result;
}

export function record_matches_step(record: Record, step: string|undefined, findDecorator=false): Boolean
{
	step = step?.toLowerCase();
	var rec_text = record.text.toLowerCase();
	if ( step == undefined || rec_text == undefined){ return false;}

	if ( step.indexOf('and') === 0){
		var sb = rec_text.split(' ')[0];
		step = step.replace('and', sb);
	}

	step = step.replace(/["'<][^ ]+["'>]/g, '_');
	rec_text = rec_text.replace(/[{<(][^ ]+[})>]/g, '_');
	if (rec_text === step){
		return true;
	}
  	return false;
}

export function get_completions(search: Search, starting: string): vscode.CompletionItem[] {
	var compls: Array<vscode.CompletionItem> = [];
	search.step_decs.forEach(element => {
		var completion_text = element.text.trim();
		completion_text = completion_text.charAt(0).toUpperCase() + completion_text.slice(1);
	
		while (completion_text.indexOf('{') >= 0){
			completion_text = completion_text.replace('{','"');
			completion_text = completion_text.replace('}','"');
		}
		
		if(!completion_text.toLocaleLowerCase().startsWith(starting.toLowerCase().trim())){return;}
		
		completion_text = completion_text.slice(starting.length);

		const snip = new vscode.CompletionItem(completion_text);
		var insert_text = completion_text;
		snip.insertText = new vscode.SnippetString(insert_text);
		var list = element.file.path.split('/');
		var fname = list[list.length-1];
		var suite = list[list.length-3];
		var doc = 'Directory: '+ suite + ".step_defs" ;
		snip.documentation = new vscode.MarkdownString(doc, true);
		snip.documentation.appendCodeblock('File: '+ fname, 'python');
		snip.documentation.appendCodeblock('\n' + insert_text);
		snip.label = completion_text.replace(starting.trim(), '');
				

		snip.kind = vscode.CompletionItemKind.Text;
		compls.push(snip);
	});
	// return all completion items as array
	return compls;
}

export function get_lines_starting(document: vscode.TextDocument, start_words: Array<string>, spl: string): Record[]
{
	var lines_count = document.lineCount;
	var i = 0;
	var records = [];
	for (i = 0; i<lines_count; i++)
	{
		var line = document.lineAt(i).text.trim();
		if (line === ""){continue;}

		var start_word = line.split(spl)[0].trim();
		if ( start_words.indexOf(start_word) >= 0){
			var rec = new Record();
			rec.text = line;
			rec.file = document.uri;
			rec.line = i;
			records.push(rec);
		}
	}
	return records;
}

export function get_lines_including(document: vscode.TextDocument, are_words: Array<string>, spl: string): Record[]
{
	var lines_count = document.lineCount;
	var i = 0;
	var records = [];
	for (i = 0; i<lines_count; i++)
	{
		var line = document.lineAt(i);
		if (line.isEmptyOrWhitespace){continue;}
		for(var iw=0; iw<are_words.length; iw++){
			if ( line.text.indexOf(are_words[iw]) >= 0){
				var rec = new Record();
				rec.text = line.text;
				rec.file = document.uri;
				rec.line = i;
				records.push(rec);
			}
		}
	}
	return records;
}

export function go_to_step_definition(step_decs: Record[]){
	// Get step text from the active text editor
	var line_num = vscode.window.activeTextEditor?.selection.start.line;
	if (line_num == undefined){
		return;
	}
	do {
		var line = vscode.window.activeTextEditor?.document.lineAt(line_num);
		if (line == undefined){return;}
		line_num -= 1;
	} while( line_num >=0 && line?.text.indexOf('|') >= 0);
	// Go to step definition
	const step = line?.text.trim().replace('  ', ' ',).replace('  ', ' ',);

	step_decs.forEach(record=>{
		if (record_matches_step(record, step)){
			vscode.workspace.openTextDocument(record.file).then((a: vscode.TextDocument) => {
				vscode.window.showTextDocument(a, 2, false).then(e => {
					select_focus_line(e, record.line);
				});
			});
		}
	});
}

export function get_doc_from_path(path: string): Thenable<vscode.TextDocument>{
	const uri = vscode.Uri.file(path);
	return vscode.workspace.openTextDocument(uri);
}

export function show_doc(path: string, col=1): Thenable<vscode.TextEditor>{
	return get_doc_from_path(path).then(doc => {
		return vscode.window.showTextDocument(doc, col, false);
	});
}

export function select_focus_line(doc: vscode.TextEditor, line: number, type=3)
{
	var pos = new vscode.Position(line, 0);
	var pos2 = new vscode.Position(line+1, 0);
	doc.selection = new vscode.Selection(pos, pos2);
	var range = new vscode.Range(pos, pos2);
	doc.revealRange(range, type);
}

export function search_feature_files_for_refs(): Thenable<Record[]>{


	return vscode.workspace.findFiles("**/features/**/*.feature").then((files ) => {
		var promises: Array<Thenable<Record[]>> = [];
		files.forEach((file) => {
			var prom = vscode.workspace.openTextDocument(file).then((document) => {
				var start_words = ['Given', 'When', 'Then', 'And'];
				var feature_refs:Array<Record> = [] = [];
				var feature_files:Array<vscode.Uri> = [] = [];

				var decs : Array<Record> =
					get_lines_starting(document, start_words, " ");
				decs.forEach(element => {
					// element.text = element.text.replace('','');
					element.text = element.text.trim();
					feature_refs.push(element);
				});
				return Promise.resolve(feature_refs);
			});

			promises.push(prom);
		});

		return Promise.all(promises).then(pr=>{
			var childs: Array<Record> = [];
			pr.forEach( arr => {
				arr.forEach(element => {
					childs.push(element);
				});
			});
			return childs;
		});
	});

}

export function search_step_files_for_decs(): Search{

	var step_decs:Array<Record> = [] = [];
	var step_files:Array<vscode.Uri> = [] = [];
	vscode.workspace.findFiles("**/step_defs/**/*.py").then((files ) => {
		files.forEach((file) => {
			if (step_files.indexOf(file) < 0 ){
				step_files.push(file);
			}
			vscode.workspace.openTextDocument(file).then((doc) => {
				var start_words = ['@given', '@when', '@then'];
				var decs : Array<Record> =
					get_lines_starting(doc, start_words, "(");
				decs.forEach(element => {

					var text = element.text;
					if(element.text.indexOf(')') < 0){
						
					text = text.replace('+', '');
					text = text.replace('\\', '');
					text += doc.lineAt(element.line + 1).text;
					text = format_decorator(text);
					}else{
						text = format_decorator(element.text);
					}
					element.text = text;
					step_decs.push(element);
				});

			});
		});
	});

	return new Search(step_decs, step_files);
}

export function delete_pytest_cache(){
	return new Promise((resolve, reject) => {
		var term = vscode.window.createTerminal('Delete Pytest Cche');
		term.sendText("Get-ChildItem -Filter '*.pyc' -Force -Recurse | Remove-Item -Force");
		resolve(true);
	});

}

vscode.workspace.onDidSaveTextDocument((document: vscode.TextDocument) => {
	if ((document.languageId === "feature"
		|| (document.languageId === "python" && document.fileName.indexOf('step_defs')>=0) )
		&& document.uri.scheme === "file") {
			vscode.commands.executeCommand('bdd.search_for_steps');
    }
});


